--[[
1) Connect to wifi
2) Set up the HTTP function
3) Set up the application check routine
4) Start polling the pins
5) Script ends but the timer continues forever

Required modules: wifi, gpio, tmr

NOTE: Lua is base-1, so all arrays start at 1, not 0.
NOTE: tables can be used as an array or an object with . : and [] syntax
NOTE: There is an important difference between . and : 
]]--

-- connect to my Wifi
wifi.setmode(wifi.STATION)
wifi.sta.config("MySSID","MyPassword")
print(wifi.sta.getip())
--example output: 10.10.10.91

-- create a crude http client
http = net.createConnection(net.TCP, false) 
-- when we receive a response, write it to the console
http:on("receive", function(conn, pl) print(pl) end)
-- this method sends the message over HTTP to a static IP and hostname
function sendMesage(message)
    http:connect(80,"121.41.33.127")
    http:send("GET / HTTP/1.1\r\nHost: www.nodemcu.com?message=" .. 
        message ..
        "\r\nConnection: keep-alive\r\nAccept: */*\r\n\r\n")
end

-- here is a list of appliances, just set the pin
appliances = {
    {
        sensorPin = 1,
        ledPin = 4,
        state = false,
        onMessage = "washer has started",
        offMessage = "washer has stopped",
        values = {}
    },
    {
        sensorPin = 2,
        ledPin = 5,
        state = false,
        onMessage = "dryer has started",
        offMessage = "dryer has stopped",
        values = {}
    }
}
readFrequency = 100 -- in ms
timeIndex = 1 -- increments every readFrequency, resets after timeSpan
timeSpan = 20 -- we averages this many interations

-- This method takes an appliance table from the list above and does all the stuff
function checkState(appliance)    
    local value = gpio.read(app.sensorPin)
    appliance.values[timeIndex] = value
    gpio.write(app.ledPin, value)

    -- average the values
    local average
    for _, v in ipairs(appliance.values) do
        if v then
            average = avarage + 1 
        else
            average = average - 1 
        end
    end
    local newState = avarage > 0

    -- stop if nothing has changed
    if newState == appliance.state then return end

    -- remember the state
    appliance.state = newState

    -- send the message
    if newState then
        sendMessage(appliance.onMessage)
    else
        sendMessage(appliance.offMessage)
    end
end

-- initialize the pins as inputs
for _, appliance in pairs(applicances) do
    gpio.mode(appliance.pin, pio.INPUT, gpio.PULLUP)
    checkState(index)
end

-- poll the sensors every [readFrequency] milliseconds to look for
-- a change in the average over [readFrequency * timeSpan] milliseconds
tmr.alarm(0,readFrequency,1,function()
    for _, appliance in pairs(applicances) do
        checkState(appliance)
    end

    timeIndex = timeIndex + 1
    if timeIndex > timeSpan then
        timeIndex = 1
    end
end)